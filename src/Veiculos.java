
public class Veiculos {
  

   boolean EmPercurso;
   
   protected boolean UsaGasolina;
   protected boolean UsaDiesel;
   protected boolean UsaAlcool;
  
   double VelMedia;
   
   double RendimentoSCG; 
   double RendimentoSCD; 
   double RendimentoSCA; 
   
   double RendimentoCCG;
   double RendimentoCCD; 
   double RendimentoCCA; 
   
   double PerdaRendimentoG;
   double PerdaRendimentoD;
   double PerdaRendimentoA;
   
   int CargaMaximaKg;
  
   
   protected void setRendimentosCC(double Carga){
    if(CargaMaximaKg<Carga){
       if(UsaGasolina==true){
         
           RendimentoCCG=RendimentoSCG-(Carga*PerdaRendimentoG);
   
                        }else{RendimentoCCG=0;}
       
       if(UsaDiesel==true){
          
           RendimentoCCD=RendimentoSCD-(Carga*PerdaRendimentoD);
     
                       }else{RendimentoCCD=0;}
       
       if(UsaAlcool==true){
         
           RendimentoCCA=RendimentoSCA-(Carga*PerdaRendimentoA);
      
                       }else{RendimentoCCA=0;}
                         }
   }
                                              
   protected void setEmPercurso(boolean a){
   EmPercurso=a;
   }

   
   protected boolean getUsaGasolina(){
   return UsaGasolina;}
   
   protected boolean getUsaDiesel(){
   return UsaDiesel;}
   
   protected boolean getUsaAlcool(){
   return UsaAlcool;}


}