
import java.util.ArrayList;


public class ListasVeiculos {
    ArrayList<Carreta> ListaCarreta;
    ArrayList<Carro> ListaCarro;
    ArrayList<Moto> ListaMoto;
    ArrayList<Van> ListaVan;
    Guardar a;
    
    public ListasVeiculos() {
        this.ListaVan = new ArrayList();
        this.ListaMoto = new ArrayList();
        this.ListaCarro = new ArrayList();
        this.ListaCarreta = new ArrayList();
        this.a=new Guardar();
    }
    
    
     
    public void criaCarreta(int b){
     int i=0;
     while(i<b){
     Carreta c=new Carreta();
     ListaCarreta.add(c);
     i++;}
     
    }
    public void CarretasExistentes(){
     int b,i=0;
     b=a.getCarretasExistentes();
     while(i<b){
     Carreta c =new Carreta();
     ListaCarreta.add(c);
     i++;}}
    
    public void criaCarro(int b){
    int i=0;
    while(i<b){
     Carro c=new Carro();
     ListaCarro.add(c);
     i++;
    }
    }  
    public void carroExistentes(){
    int b,i=0;
    b=a.getCarrosExistentes();
    while(i<b){
     Carro c=new Carro();
     ListaCarro.add(c);
     i++;
    }
    }  
    
    public void criaMoto(int b){
        int i=0;
     while(i<b){   
     Moto m=new Moto();
     ListaMoto.add(m);
     i++;}   
    }
    public void MotosExistentes( ){
     int b,i=0;
     b=a.getMotosExistentes();
     while(i<b){   
     Moto m=new Moto();
     ListaMoto.add(m);
     i++;}   
    }
    
    public void criaVan(int b){
     int i=0;
     while(i<b){
     Van v =new Van();
     ListaVan.add(v);
     i++;}}
    public void VansExistentes(){
     int b,i=0;
     b=a.getVansExistentes();
     while(i<b){
     Van v =new Van();
     ListaVan.add(v);
     i++;}}
    
    
     
    /**Muda o status de uma Carreta da ListaCarreta  EmPerurso para true
     *
     */
    public void setCarretaEmPercurso(){
    int i=0;
        if(!ListaCarreta.isEmpty()){
            while(ListaCarreta.get(i).EmPercurso==true && ListaCarreta.size()>i){
            i++;
            }
            if(i<ListaCarreta.size()){
            ListaCarreta.get(i).EmPercurso=true;
            }
    }
}

    /**Muda o status de um Carro da ListaCarro  EmPerurso para true
     *
     */
    public void setCarroEmPercurso(){
        int i=0;
        if(!ListaCarro.isEmpty()){
            while(ListaCarro.get(i).EmPercurso==true && ListaCarro.size()>i){
            i++;
            }
            if(i<ListaCarro.size()){
            ListaCarro.get(i).EmPercurso=true;
            }
    }
}
      
    /**Muda o status de uma Moto da ListaMoto  EmPerurso para true
     *
     */    
    public void setMotoEmPercurso(){
      int i=0;
        if(!ListaMoto.isEmpty()){
            while(ListaMoto.get(i).EmPercurso==true && ListaMoto.size()>i){
            i++;
            }
            if(i<ListaMoto.size()){
            ListaMoto.get(i).EmPercurso=true;
            }
    }
 }
    
    /**Muda o status de uma Van da ListaVan  EmPerurso para true
     *
     */
    public void setVanEmPercurso(){
      int i=0;
        if(!ListaVan.isEmpty()){
            while(ListaVan.get(i).EmPercurso==true && ListaVan.size()>i){
            i++;
            }
            if(i<ListaVan.size()){
            ListaVan.get(i).EmPercurso=true;
            }
    }
}
    
    
    
    public int getCarretaEmPercurso(){
     int i=0,soma=0;
     
     while(ListaCarreta.size()>i){
      if(ListaCarreta.get(i).EmPercurso=true){
      soma++;}
      i++;
     
     }
    
    return soma;}
    
    public int getCarroEmPercurso(){
     int i=0,soma=0;
     
     while(ListaCarro.size()>i){
      if(ListaCarro.get(i).EmPercurso=true){
      soma++;}
      i++;
     
     }
    
    return soma;}    

    public int getMotoEmPercurso(){
     int i=0,soma=0;
     
     while(ListaMoto.size()>i){
      if(ListaMoto.get(i).EmPercurso=true){
      soma++;}
      i++;
     
     }
    
    return soma;}     
    
    public int getVanEmPercurso(){
     int i=0,soma=0;
     
     while(ListaVan.size()>i){
      if(ListaVan.get(i).EmPercurso=true){
      soma++;}
      i++;
     
     }
    
    return soma;}  
     
    public int getVeiculosEmPercurso(){
     int soma;
     soma=(getCarretaEmPercurso()+getCarroEmPercurso()+getMotoEmPercurso()+getVanEmPercurso());
    
    return soma;}
    
    public int getVeiculosExistentes(){
    int soma;
    soma=(ListaCarreta.size()+ListaCarro.size()+ListaMoto.size()+ListaVan.size());
        return soma;}
    
}
