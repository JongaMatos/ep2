/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joaog
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JOptionPane;
public class Guardar {
    Path caminho;
    byte[] texto;
    String leitura;
    
    /**
     *Adiciona os carros armazenados
     * 
     */
    public int getCarrosExistentes(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/CarrosExistentes.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}
    public void setCarrosExistentes(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/CarrosExistentes.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
     /**
     *Adiciona os carros armazenados em frete
     * 
     */
    public int getCarrosEmFrete(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/CarrosEmFrete.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}
     public void setCarrosEmFrete(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/CarrosEmFrete.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
      /**
     *Adiciona as carretas armazenados
     * 
     */
    public int getCarretasExistentes(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/CarretasExistentes.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}    
    public void setCarretasExistentes(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/CarretasExistentes.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
     /**
     *Adiciona as carretas armazenadas em frete
     * 
     */
    public int getCarretasEmFrete(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/CarretasEmFrete.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}
    public void setCarretasEmFrete(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/CarretasEmFrete.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
    /**
     *Adiciona as motos armazenadas
     * 
     */
    public int getMotosExistentes(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/MotosExistentes.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}   
    public void setMotosExistentes(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/MotosExistentes.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
    /**
     *Adiciona as motos armazenadas em frete
     * 
     */
    public int getMotosEmFrete(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/MotosEmFrete.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}    
    public void setMotosEmFrete(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/MotosEmFrete.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
    /**
     *Adiciona as vans armazenadas
     * 
     */
    public int getVansExistentes(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/VansExistentes.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}
    public void setVansExistentes(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/VansExistentes.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
    /**
     *Adiciona as vans armazenadas em frete
     * 
     */
    public int getVansEmFrete(){
        int cE;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/VansEmFrete.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          cE=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            cE=0;
                              }
   return cE;}
    public void setVansEmFrete(String valor){
        
        String A=System.getProperty("user.dir")+"/Armazenados/VansEmFrete.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
    
    public int getMargemAtual(){        
       int ML;
      caminho=Paths.get(System.getProperty("user.dir")+"/Armazenados/AtualMargemDeLucro.txt");
     try{ texto=Files.readAllBytes(caminho);
          leitura=new String(texto);
          ML=Integer.parseInt(leitura);          
          
        }catch(Exception erro){
            ML=0;
                              }
   return ML;}  
    public void setMargemNova(String valor){
        //Path c=Paths.get(System.getProperty("user.dir")+"/Armazenados/AtualMargemDeLucro.txt");
        
        String A=System.getProperty("user.dir")+"/Armazenados/AtualMargemDeLucro.txt";
        File t=new File(A);
        int aux;
        if(t.exists()){t.delete();}
        try {                
            
            t.createNewFile();
        
         FileWriter fw;
            fw = new FileWriter(t);
         BufferedWriter bw;
            bw = new BufferedWriter(fw);
           
         bw.write(valor);
         
            bw.close();
        }catch(IOException e){
            
        
        }
        
    
    
    
    }
    
    /**
     *zera tds os arquivos q armazenam veiculos
     */
    public void zerar(){
    setCarrosExistentes("0");
    setCarretasExistentes("0");
    setVansExistentes("0");
    setMotosExistentes("0");
    
    }
    


}
    
   

    
    
    
    
    
    
    
    
    

